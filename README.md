## @universis/ngx-profiles

Implementation of [@universis/profiles](https://gitlab.com/universis/universis-profiles) for client applications.

### Usage

Install `@universis/ngx-profiles`

    npm i @universis-ngx-profiles

Import `NgxProfilesModule` in your application module

        @NgModule({
            declarations: [
                ...
            ],
            imports: [
                ...,
                NgxProfilesModule.forRoot()
            ]
        })
        export class AppModule {

        }

Provide `@universis/common#UserService` by using `NgxProfilesUserService`

        import {UserService} from '@universis/common';
        import { NgxProfilesModule, NgxProfilesUserService } from '@universis/ngx-profiles';

        @NgModule({
            declarations: [
                ...
            ],
            imports: [
                ...,
                NgxProfilesModule.forRoot()
            ],
            providers: [
                ...,
                {
                    provide: UserService,
                    useClass: NgxProfilesUserService
                }
            ]
        })
        export class AppModule {

        }

`NgxProfilesService.getStudentProfiles()` returns an array of available student profiles which are student records associated with the current user. Use this collection -create a dropdown list or a modal window- to allow user select and activate one of this profiles

        ngOnInit() {
            // validate support of student profiles
            this.profiles.supported().then((supportsProfiles) => {
                if (supportsProfiles) {
                    this.profiles.getStudentProfiles().then((linkProfiles) => {
                    this.linkProfiles = linkProfiles;
                    });
                }
            });
        }

Finally set active student profile by using `NgxProfilesService.setStudentProfile(linkProfile)` method.

        ...
        select(linkProfile: LinkProfile): void {
            // set profile
            this.profiles.setStudentProfile(linkProfile);
            // navigate to home
            this.router.navigate(['/']).then(() => {
            // do nothing
            });
        }

### Development guidelines

Clone repository as submodule of an existing universis application e.g. universis-students

        git submodule add https://gitlab.com/universis/ngx-profiles.git projects/ngx-profiles

Modify angular.json to include new library under `projects` node

        {
            ...
            "projects": [
                ...
                "ngx-profiles": {
                    "root": "projects/ngx-profiles",
                    "sourceRoot": "projects/ngx-profiles/src",
                    "projectType": "library",
                    "prefix": "lib",
                    "architect": {
                        "build": {
                        "builder": "@angular-devkit/build-ng-packagr:build",
                        "options": {
                            "tsConfig": "projects/ngx-profiles/tsconfig.lib.json",
                            "project": "projects/ngx-profiles/ng-package.json"
                        }
                        },
                        "test": {
                        "builder": "@angular-devkit/build-angular:karma",
                        "options": {
                            "main": "projects/ngx-profiles/src/test.ts",
                            "tsConfig": "projects/ngx-profiles/tsconfig.spec.json",
                            "karmaConfig": "projects/ngx-profiles/karma.conf.js"
                        }
                        },
                        "lint": {
                        "builder": "@angular-devkit/build-angular:tslint",
                        "options": {
                            "tsConfig": [
                                "projects/ngx-profiles/tsconfig.lib.json",
                                "projects/ngx-profiles/tsconfig.spec.json"
                                ],
                                "exclude": [
                                "**/node_modules/**"
                                ]
                            }
                        }
                    }
                }
            ]
        }

Build `@universis/ngx-profiles` and link library

    npm run build ngx-profiles
    npm link dist/ngx-profiles --no-save
