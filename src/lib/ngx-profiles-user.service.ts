import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ConfigurationService, UserService } from '@universis/common';

@Injectable()
export class NgxProfilesUserService extends UserService {

    private _currentContext: AngularDataContext;

    constructor(_context: AngularDataContext,
        _configurationService: ConfigurationService) {
            super(_context, _configurationService);
            this._currentContext = _context;
        }
    getUserSync() {
        const user = super.getUserSync();
        if (user != null) {
            // try to find if user has profile property
            if (user.profile != null) {
                // and set context header to this profile
                this._currentContext.getService().setHeader('Accept-Profile', user.profile);
            }
        }
        // finally return user
        return user;
    }
}
