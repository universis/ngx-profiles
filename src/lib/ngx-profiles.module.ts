import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { MostModule } from '@themost/angular';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NgxProfilesUserService } from './ngx-profiles-user.service';
import { NgxProfilesService } from './ngx-profiles.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    MostModule,
    BsDropdownModule
  ],
  declarations: [
  ],
  exports: [
  ]
})
export class NgxProfilesModule {

  constructor( @Optional() @SkipSelf() parentModule: NgxProfilesModule) {
    //
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: NgxProfilesModule,
      providers: [
        NgxProfilesService
      ]
    };
  }
}
